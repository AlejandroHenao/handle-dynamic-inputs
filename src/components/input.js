import React from 'react'
import Autocomplete from './autocomplete'
class Input extends React.Component{
    constructor(props){
        super(props);
        this.state={
        
        }
    }
    
    render(){
        return(
            <Autocomplete 
                dataSource={[]}
                name={this.props.name}
                getValue={e=>this.props.getValue(e)}
            />
        )
    }
}
export default Input 
