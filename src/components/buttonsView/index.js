import React from 'react'
import Input from './../input'
import InputList from './..//inputList'

   
class ButtonsView extends React.Component{
    constructor(props){
        super(props);
        this.state={
            words:{},
            inputs:[]
        };
        
        this.onAddWord=this.onAddWord.bind(this);
        this.onSubmit=this.onSubmit.bind(this);
        this.getInputsValue=this.getInputsValue.bind(this);
        this.index=-1;
         this.inputComponent=name=>(
        {component:<Input name={name} getValue={this.getInputsValue}/>,
        id:name}
    )
 
    }
    onAddWord(){
        this.index+=1;
        this.setState({
            inputs:this.state.inputs.concat([this.inputComponent('word'+this.index)])
        })
    }
    getInputsValue(event){
        let words=this.state.words
        words[event.target.name]=event.target.value
        this.setState({words})
    }
    onSubmit(){
        console.log(this.state)
    }
    render(){
        return(
            <React.Fragment>
                <button
                    onClick={this.onAddWord}
                >
                    Add word
                </button>
                <button
                    onClick={this.onSubmit}
                >
                    Submit
                </button>
                <InputList inputs={this.state.inputs}/>
            </React.Fragment>
        );
    }   
}
export default ButtonsView
