import React from 'react'

const InputList= ({inputs})=>{
    return(
        inputs.map(item=>{
            return(
                <div key={item.id}>
                    {item.component}
                </div>    
            )
        })
    )
}
export default InputList
