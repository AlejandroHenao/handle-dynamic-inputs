import React from 'react'
import Sugestion from './sugestion'
class SugestionList extends React.Component{
    constructor(props){
        super(props);
        this.focus=null;
    }

    render=()=>{
        if (this.props.open===false) {
            return null
        }else{ 
            return(
                this.props.options.slice(0,this.props.max).map((item,index)=>{
                    if (index===this.props.keySelect) {
                        this.focus=this.props.keyhover;
                    }else{
                       this.focus='';
                    }
                    return(
                        <Sugestion key={item.word} 
                            option={item.word}
                            clickSubmit={(v)=>this.props.clickSubmit(v)}
                            sugestionClass={this.props.suggestionClass}
                            keyhover={this.focus}
                        />
                    )
                })
            )    
        }
    }    

}
export default SugestionList 
