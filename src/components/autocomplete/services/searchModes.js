export const wholeWord=(in_request,in_data)=>{
    const analize=(request,data)=>{
        if (request==null) {
            return('')
        }else{
        return{
            wordIndex:data.toLowerCase().indexOf(request.toLowerCase()),//index of start match
            match:data.toLowerCase().includes(request.toLowerCase()) //return true if match
        }
        }
    }
    let resultObject=[];
    for (let index = 0; index < in_data.length; index++) {
        let itemAnalized=analize(in_request,in_data[index]);
        if (itemAnalized.match) {
            resultObject.push({
                startMatch:itemAnalized.wordIndex,
                word:in_data[index],
            })
        }
    }
    return(resultObject)
}
