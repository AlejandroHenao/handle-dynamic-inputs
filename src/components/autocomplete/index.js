import React,{Component} from 'react'
import SugestionList from './sugestions'
import {wholeWord} from './services/searchModes'
import {DEFAULT,DFOCUS} from './constants'
import './styles.css'
class Autocomplete extends Component{
    constructor(props){
        super(props);
        this.state={
            inputValue:'',
            keySelect:0,    
            openSugestions:false,
            inputCanBlur:true
        }
        this.handleKeyDown=this.handleKeyDown.bind(this);
        this.handleInputUpdate=this.handleInputUpdate.bind(this);
        this.handleClickSubmit=this.handleClickSubmit.bind(this);
        this.handleBlurComponent=this.handleBlurComponent.bind(this);

        this.keyhover=this.props.keyHoverClass?this.props.keyHover:DFOCUS;
        this.suggestionClass=this.props.suggestionClass?this.props.suggestionsClass:DEFAULT;
        this.maxSuggestion=this.props.max?this.props.max:5
    }
    handleBlurComponent(){
        if (this.state.inputCanBlur===true) {
            this.setState({openSugestions:false
            })
        }
    }
    handleInputUpdate(event){
        this.setState(
            {inputValue:event.target.value,
            keySelect:0}
        )
        if (event.target.value.length!==0) {
            this.setState({openSugestions:true})
        }else{this.setState({openSugestions:false})}
        this.props.getValue(event)
    }
    handleClickSubmit(value){
        this.setState(
            {inputValue:value,
            openSugestions:false}
        )
    }
    handleKeyDown(key){
        if (key.keyCode===40) {//Down Key
            this.setState(prevState=>{return{keySelect:prevState.keySelect+1}})
            if (this.state.keySelect>this.props.dataSource.length-2) {
                this.setState({keySelect:0})
            }
            if (!this.state.openSugestions) {
                this.setState({openSugestions:true})
            }
        }
        else if (key.keyCode===38) {//Up Key
            this.setState(prevState=>{return{keySelect:prevState.keySelect-1}})
            if (this.state.keySelect<=0) {
                this.setState({openSugestions:false})
            }
            if (this.state.keySelect===-2) {
                this.setState({keySelect:-1})
            }
        }
        else if (key.keyCode===13) {//Enter Key
            this.setState(
                {inputValue:this.props.dataSource[this.state.keySelect],
                openSugestions:false}
            );
        }
    }   
    componentDidUpdate(){
        if(this.props.getValue){
            //this.props.getValue(this.state.inputValue)
        };
    }
    render(){
        return(
            <div style={{position:'relative',width:'100%'}}>
                <input type='text'autoFocus={true}
                    name={this.props.name}
                    onKeyDown={this.handleKeyDown}    
                    onChange={this.handleInputUpdate}
                    value={this.state.inputValue}
                    style={{width:'100%'}}
                    onBlur={this.handleBlurComponent}
                    className={this.props.ClassName}
                />
                <div 
                    onMouseOver={()=>this.setState({inputCanBlur:false})}
                    onMouseOut={()=>this.setState({inputCanBlur:true})}
                    style={{
                        position:'absolute',
                        zIndex:'1000',
                        top:'100%',left:'0',right:'0'
                    }}
                >
                    <SugestionList
                        options={wholeWord(this.state.inputValue,this.props.dataSource)}
                        clickSubmit={this.handleClickSubmit}
                        keySelect={this.state.keySelect}
                        open={this.state.openSugestions}
                        keyhover={this.keyhover}
                        suggestionClass={this.suggestionClass}
                        max={this.maxSuggestions}
                    />
                </div>    
            </div>
            
        )
    }
}
export default Autocomplete 
